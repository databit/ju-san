# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import tools
from lxml import etree
from openerp.osv import fields, osv
from datetime import datetime
from dateutil.relativedelta import relativedelta

class aging_partner_reporting(osv.osv):
    _name = "aging.partner.reporting"
    _description = "Aging Partner Reporting"
    _auto = False
    
    def function_aging(self, cr, uid, ids, fields_name, args, context=None):
        obj_move = self.pool.get('account.move.line')
        date = context.get('date', datetime.now().strftime('%Y-%m-%d'))
        period_length = context.get('period_length', False)
        start = datetime.strptime(context.get('date', False), "%Y-%m-%d")
        self.date_from = context.get('date', False)
        self.direction = context.get('direction', False)
        self.result_selection = context.get('result_selection', False)
        self.query = obj_move._query_get(cr, uid, obj='l', context=None)
        #print "start",start
        result = {}
        form = {}
        for a in range(5)[::-1]:
            stop = start - relativedelta(days=period_length)
            form[str(a)] = {
                'name': (a!=0 and (str((5-(a+1)) * period_length) + '-' + str((5-a) * period_length)) or ('+'+str(4 * period_length))),
                'stop': start.strftime('%Y-%m-%d'),
                'start': (a!=0 and stop.strftime('%Y-%m-%d') or False),
            }
            start = stop - relativedelta(days=1)
            
        for line in self.browse(cr, uid, ids):
            partner_ids = [line.partner_id.id]
            line_ids = [line.id]
            move_state = ['posted']
            account_type = [self.result_selection]
            totals = {}
            cr.execute('SELECT l.partner_id, SUM(l.debit-l.credit) \
                        FROM account_move_line AS l, account_account, account_move am \
                        WHERE (l.account_id = account_account.id) AND (l.move_id=am.id) \
                        AND (l.id IN %s) \
                        AND (am.state IN %s)\
                        AND (account_account.type IN %s)\
                        AND (l.partner_id IN %s)\
                        AND ((l.reconcile_id IS NULL)\
                        OR (l.reconcile_id IN (SELECT recon.id FROM account_move_reconcile AS recon WHERE recon.create_date > %s )))\
                        AND ' + self.query + '\
                        AND account_account.active\
                        AND (l.date <= %s)\
                        GROUP BY l.partner_id ', (tuple(line_ids), tuple(move_state), tuple(account_type), tuple(partner_ids), self.date_from, self.date_from,))
            t = cr.fetchall()
            for i in t:
                totals[i[0]] = i[1]
            print "-------------",totals
            history = []
            for i in range(5):
                args_list = (tuple(line_ids), tuple(move_state), tuple(account_type), tuple(partner_ids),self.date_from,)
                dates_query = '(COALESCE(l.date_maturity,l.date)'
                #print "==========",form[str(i)]['start']
                if form[str(i)]['start'] and form[str(i)]['stop']:
                    dates_query += ' BETWEEN %s AND %s)'
                    args_list += (form[str(i)]['start'], form[str(i)]['stop'])
                elif form[str(i)]['start']:
                    dates_query += ' >= %s)'
                    args_list += (form[str(i)]['start'],)
                else:
                    dates_query += ' <= %s)'
                    args_list += (form[str(i)]['stop'],)
                args_list += (self.date_from,)
                #print "args_list",dates_query,args_list
                cr.execute('''SELECT l.partner_id, SUM(l.debit-l.credit), l.reconcile_partial_id
                        FROM account_move_line AS l, account_account, account_move am 
                        WHERE (l.account_id = account_account.id) AND (l.move_id=am.id)
                            AND (l.id IN %s)
                            AND (am.state IN %s)
                            AND (account_account.type IN %s)
                            AND (l.partner_id IN %s)
                            AND ((l.reconcile_id IS NULL)
                              OR (l.reconcile_id IN (SELECT recon.id FROM account_move_reconcile AS recon WHERE recon.create_date > %s )))
                            AND account_account.active
                            AND ''' + dates_query + '''
                        AND (l.date <= %s)
                        GROUP BY l.partner_id, l.reconcile_partial_id''', args_list)
                partners_partial = cr.fetchall()
                partners_amount = dict((i[0],0) for i in partners_partial)
                for partner_info in partners_partial:
                    if partner_info[2]:
                        # in case of partial reconciliation, we want to keep the left amount in the oldest period
                        cr.execute('''SELECT MIN(COALESCE(date_maturity,date)) FROM account_move_line WHERE reconcile_partial_id = %s''', (partner_info[2],))
                        date = cr.fetchall()
                        partial = False
                        if 'BETWEEN' in dates_query:
                            partial = date and args_list[-3] <= date[0][0] <= args_list[-2]
                        elif '>=' in dates_query:
                            partial = date and date[0][0] >= form[str(i)]['start']
                        else:
                            partial = date and date[0][0] <= form[str(i)]['stop']
                        if partial:
                            # partial reconcilation
                            limit_date = 'COALESCE(l.date_maturity,l.date) %s %%s' % '<=' if self.direction == 'past' else '>='
                            cr.execute('''SELECT SUM(l.debit-l.credit)
                                               FROM account_move_line AS l, account_move AS am
                                               WHERE l.move_id = am.id AND am.state in %s
                                               AND l.reconcile_partial_id = %s
                                               AND ''' + limit_date, (tuple(move_state), partner_info[2], self.date_from))
                            unreconciled_amount = cr.fetchall()
                            partners_amount[partner_info[0]] += unreconciled_amount[0][0]
                    else:
                        partners_amount[partner_info[0]] += partner_info[1]
                history.append(partners_amount)
            print "histor===============",history
            result[line.id] =  {'aging5' : history[0].values() and history[0].values()[0],
                                'aging4' : history[1].values() and history[1].values()[0],
                                'aging3' : history[2].values() and history[2].values()[0],
                                'aging2' : history[3].values() and history[3].values()[0],
                                'aging1' : history[4].values() and history[4].values()[0],
                                'total' : totals.values() and totals.values()[0]}
        return result
    
    _columns = {
        'date': fields.date('Date', readonly=True),  # TDE FIXME master: rename into date_order
        'partner_id' : fields.many2one('res.partner', 'Partner', readonly=True),
        'move_id' : fields.many2one('account.move', '#Move', readonly=True),
        'company_id' : fields.many2one('res.company', 'Company', readonly=True),
        'aging1' : fields.function(string='Aging1', fnct=function_aging, type='float', method=True, store=False, multi='aging'),
        'aging2' : fields.function(string='Aging2', fnct=function_aging, type='float', method=True, store=False, multi='aging'),
        'aging3' : fields.function(string='Aging3', fnct=function_aging, type='float', method=True, store=False, multi='aging'),
        'aging4' : fields.function(string='Aging4', fnct=function_aging, type='float', method=True, store=False, multi='aging'),
        'aging5' : fields.function(string='Aging5', fnct=function_aging, type='float', method=True, store=False, multi='aging'),
        'total' : fields.function(string='Total', fnct=function_aging, type='float', method=True, store=False, multi='aging'),
        'balance' : fields.float('Balance', readonly=True),
    }
    def _select(self):
        select_str = """
             SELECT l.id as id, 
                l.date as date, 
                l.partner_id as partner_id, 
                l.move_id as move_id,
                l.company_id as company_id,
                SUM(l.debit-l.credit) as balance
        """
        return select_str
 
    def _from(self):
        from_str = """
            account_move_line AS l, account_account, account_move am
        """
        return from_str
 
    def _where(self):
        where_by_str = """
            (l.account_id = account_account.id) AND (l.move_id=am.id)
                AND (am.state IN ('posted'))
                AND (account_account.type IN ('receivable'))
                AND ((l.reconcile_id IS NULL)
                  OR (l.reconcile_id IN (SELECT recon.id FROM account_move_reconcile AS recon WHERE recon.create_date > '2015-11-18' )))
                AND account_account.active
            AND (l.date <= '2015-11-18')
        """
        return where_by_str
     
    def _group_by(self):
        group_by_str = """ 
            GROUP BY l.id,
                l.date,
                l.move_id,
                l.partner_id,
                l.company_id"""
        return group_by_str
 
 
    def init(self, cr):
        # self._table = sale_report
        tools.drop_view_if_exists(cr, self._table)
        cr.execute("""CREATE or REPLACE VIEW %s as (
            %s
            FROM %s
            WHERE %s
            %s
            )""" % (self._table, self._select(), self._from(), self._where(), self._group_by()))
         
      
    def fields_view_get(self, cr, uid, view_id=None, view_type='form', context=None, toolbar=False, submenu=False):
        res = super(aging_partner_reporting, self).fields_view_get(cr, uid, view_id, view_type, context, toolbar, submenu)
        if view_type == 'tree':
            period_length = context.get('period_length', False)
            aging_label = {}
            if period_length: 
                doc = etree.XML(res['arch'])
                for interval in range(1,6):
                    aging_label['aging%s' % interval] = '%s - %s' % (str(period_length * (interval-1)), str(period_length * (interval)))
                    if interval == 5:
                        aging_label['aging5'] = '+%s' % (str(period_length * (4))) 
                    for node in doc.xpath("//field[@name='aging%s']" % interval):
                        node.set('string',aging_label['aging%s' % interval])
                res['arch'] = etree.tostring(doc) 
        return res

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
