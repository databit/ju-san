# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp.osv import osv, fields

class wizard_aging_partner_reporting(osv.osv_memory):
    _name = 'wizard.aging.partner.reporting'
    _description = 'Wizard Query Receivable Aging'
   
    def default_company_id(self, cr, uid, context={}):
        obj_user = self.pool.get('res.users')
        user = obj_user.browse(cr, uid, [uid])[0]
        return user.company_id.id

    _columns =  {
        'company_id' : fields.many2one(string='Company', obj='res.company', required=True),
        'date' : fields.date(string='Start Date', required=True),
        'result_selection': fields.selection(string='Aged Type', selection=[('receivable','Receivable'),('payable','Payable')], required=True),
        'direction' : fields.selection(string='Direction', selection=[('past','Past'),('future','Future')], required=True),
        'period_length' : fields.integer(string='Period Length (days)', required=True),
    }

    _defaults = {
        'company_id' : default_company_id,
        'result_selection' : 'receivable',
        'direction' : 'past',
        'period_length' : 30,
        'date': fields.datetime.now,
    }

    def button_query_report(self, cr, uid, ids, data, context=None):
        obj_data = self.pool.get('ir.model.data')
        obj_line = self.pool.get('account.move.line')
        wizard = self.browse(cr, uid, ids)[0]
        self.query = obj_line._query_get(cr, uid, obj='l', context=None)
        #print "wizard.date",wizard.date
        if wizard.result_selection == 'receivable':
            name = 'Aged Receivable'
        else:
            name = 'Aged Payable'
        line_ids = obj_line.search(cr, uid, [('move_id.state','=','posted'),('date','<=',wizard.date),('account_id.type','=',wizard.result_selection),('reconcile_id','=',None),'|',('reconcile_partial_id','=',None),('journal_id.type','not in',('cash','bank'))])
        #print "line_ids",line_ids
        #if line_ids:
        view_id = obj_data.get_object_reference(cr, uid, 'dos_account_reporting_view', 'view_aging_partner_reporting_tree')[1]
        if context is None:
            context = {} 
        return  {
            'type': 'ir.actions.act_window',
            'name' : name,
            'res_model' : 'aging.partner.reporting',
            'view_type' : 'form',
            'view_mode' : 'tree',
            'view_id' : view_id,
            'target' : 'current',
            'context' : {'result_selection':wizard.result_selection,'period_length' : wizard.period_length, 'date' : wizard.date, 'direction': wizard.direction},#{'period_length' : wizard.period_length, 'date' : wizard.date},
            'domain' : [('id','in',line_ids)],
        }

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
