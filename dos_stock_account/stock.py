# -*- encoding: utf-8 -*-
##############################################################################
#    
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp import SUPERUSER_ID, api
import logging
_logger = logging.getLogger(__name__)

class stock_move(osv.Model):
    _inherit = "stock.move"
    
#     def action_done(self, cr, uid, ids, context=None):
#         #context.update({'date'})
#         #self.product_price_update_before_done(cr, uid, ids, context=context)
#         res = super(stock_move, self).action_done(cr, uid, ids, context=context)
#         #self.product_price_update_after_done(cr, uid, ids, context=context)
#         print "action_done",res
#         return res
    
    def product_price_update_before_done(self, cr, uid, ids, context=None):
        product_obj = self.pool.get('product.product')
        product_cogs_obj = self.pool.get('product.cogs.price')
        tmpl_dict = {}
        for move in self.browse(cr, uid, ids, context=context):
            print "product_price_update_before_done",move,context
            #adapt standard price on incomming moves if the product cost_method is 'average'
            if (move.location_id.usage == 'supplier') and (move.product_id.cost_method == 'average'):
                product = move.product_id
                prod_tmpl_id = move.product_id.product_tmpl_id.id
                qty_available = move.product_id.product_tmpl_id.qty_available
                amount_unit = product.standard_price
                if tmpl_dict.get(prod_tmpl_id):
                    product_avail = qty_available + tmpl_dict[prod_tmpl_id]
                else:
                    tmpl_dict[prod_tmpl_id] = 0
                    product_avail = qty_available
                if product_avail <= 0:
                    new_std_price = move.price_unit
                else:
                    # Get the standard price
                    amount_unit = product.standard_price
                    new_std_price = ((amount_unit * product_avail) + (move.price_unit * move.product_qty)) / (product_avail + move.product_qty)
                tmpl_dict[prod_tmpl_id] += move.product_qty
                # Write the standard price, as SUPERUSER_ID because a warehouse manager may not have the right to write on products
                ctx = dict(context or {}, force_company=move.company_id.id)
                #updatenya tidak ke standard price lagi
                #product_obj.write(cr, SUPERUSER_ID, [product.id], {'standard_price': new_std_price}, context=ctx)
                vals_cogs = {
                    'product_tmpl_id': product.product_tmpl_id and product.product_tmpl_id.id,
                    'stock_move_id': move.id,
                    'name':fields.date.context_today(self, cr, uid, context=context), #date seharusnya bisa backdate
                    'qty_before':product_avail,
                    'qty_new':move.product_qty,
                    'cogs_before':amount_unit,
                    'cogs_new':move.price_unit,
                }
                product_cogs_obj.create(cr, SUPERUSER_ID, vals_cogs)
                
                
                