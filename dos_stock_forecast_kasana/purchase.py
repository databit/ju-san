# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>). All Rights Reserved
#    $Id$
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from datetime import datetime
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import openerp.addons.decimal_precision as dp


class product_category(osv.osv):
    _inherit = "product.category"    
    def _get_children_and_consol(self, cr, uid, ids, context=None):
        #this function search for all the children and all consolidated children (recursively) of the given account ids
        ids2 = self.search(cr, uid, [('parent_id', 'child_of', ids)], context=context)
        ids3 = []
        for rec in self.browse(cr, uid, ids2, context=context):
            for child in rec.child_id:
                ids3.append(child.id)
        if ids3:
            ids3 = self._get_children_and_consol(cr, uid, ids3, context)
        return ids2 + ids3
    
product_category()

class product_template(osv.osv):
    _inherit = "product.template"
    _description = "Product Template"
    _columns = {
        'partner_forecast_id': fields.many2one('res.partner', 'Supplier', required=False),
        'buffer_available': fields.float('Buffer Quantity', digits_compute=dp.get_precision('Product Unit of Measure'))
    }
    
class purchase_order(osv.osv):
    _inherit = "purchase.order"
    _description = "Purchase Order"
    _columns = {
        'forecast_id': fields.many2one('stock.forecast', 'Stock Forecast', required=False),
    }
    

class purchase_order_line(osv.osv):
    _inherit = "purchase.order.line"
    _description = "Purchase Order Lines"
    _columns = {
        'forecast_line_id': fields.many2one('forecast.line', 'Forecast Product', required=False),
    }
    