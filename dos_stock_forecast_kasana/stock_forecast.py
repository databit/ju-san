import time
from datetime import datetime
from dateutil.relativedelta import relativedelta
from openerp import models, fields, api, _
from openerp.osv import fields, osv
import openerp.addons.decimal_precision as dp
from openerp.tools.translate import _


class stock_forecast(osv.osv):
    _name        = 'stock.forecast'
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _description = "Forecast Stock untuk EPN"
    _columns    = {
           'name'       : fields.char('Name', size=64, readonly=True, states={'draft':[('readonly',False)]}),
           'create_by'  : fields.many2one('res.users', "Created By", readonly=True, states={'draft':[('readonly',False)]}),
           'date_create': fields.date('Date Create', readonly=True, states={'draft':[('readonly',False)]}),
           'date_from'  : fields.date('Date From', readonly=True, states={'draft':[('readonly',False)]}),
           'date_to'    : fields.date('Date To', readonly=True, states={'draft':[('readonly',False)]}),
           'product_category_id'   : fields.many2one('product.category', "Product Category", readonly=True, states={'draft':[('readonly',False)]}),
           'product_id'         : fields.many2one('product.product', "Product", readonly=True, states={'draft':[('readonly',False)]}),
           'warehouse_id'  : fields.many2one('stock.warehouse','Warehouse', readonly=True, states={'draft':[('readonly',False)]}),
           'location_id': fields.many2one('stock.location', 'Inventoried Location', readonly=True, states={'draft':[('readonly',False)]}),
           'procurement_id': fields.many2one('procurement.order', 'Procurement', ondelete='set null', copy=False),
           'forecast_lines' : fields.one2many('forecast.line','forecast_id',"Forecast Line", readonly=True, states={'draft':[('readonly',False)]}),
           'company_id': fields.many2one('res.company', 'Company', required=True),
           'description': fields.text('Description'),
           'picking_type_id': fields.many2one('stock.picking.type', 'Picking Type', required=True),
           'state': fields.selection([('draft','Draft'),('confirm','Confirm'),('close','Close'),('cancel','Cancel')], 'Status', required=True, copy=False),
    }
    
    def _get_picking_in(self, cr, uid, context=None):
        obj_data = self.pool.get('ir.model.data')
        return obj_data.get_object_reference(cr, uid, 'stock', 'picking_type_in')[1]
    
    _defaults = {
        'state': 'draft',
        'date_create': fields.datetime.now,
        'create_by': lambda obj, cr, uid, context: uid,
        'name': lambda obj, cr, uid, context: '/',
        'company_id': lambda self, cr, uid, c: self.pool.get('res.company')._company_default_get(cr, uid, 'stock.forecast', context=c),
        'picking_type_id': _get_picking_in,
    }
    
    def unlink(self, cr, uid, ids, context=None):
        for forecast in self.browse(cr, uid, ids, context=context):
            if forecast.forecast_lines:
                for line in forecast.forecast_lines:
                    if line.move_check:
                        raise osv.except_osv(_('Error!'), _('You cannot delete/load an forecast that contains posted forecast lines.'))
        return super(stock_forecast, self).unlink(cr, uid, ids, context=context)
        
    def validate(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        return self.write(cr, uid, ids, {'state':'confirm'}, context)

    def set_to_close(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state': 'close'}, context=context)

    def set_to_draft(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state': 'draft'}, context=context)
    
    def set_to_cancel(self, cr, uid, ids, context=None):
        return self.write(cr, uid, ids, {'state': 'cancel'}, context=context)

    def _prepare_purchase_order(self, cr, uid, forecast, supplier, context=None):
        supplier_pricelist = supplier.property_product_pricelist_purchase
        return {
            'origin': forecast.name,
            'date_order': forecast.date_create or fields.datetime.now(),
            'partner_id': supplier.id,
            'pricelist_id': supplier_pricelist.id,
            'currency_id': supplier_pricelist and supplier_pricelist.currency_id.id or forecast.company_id.currency_id.id,
            'location_id': forecast.procurement_id and forecast.procurement_id.location_id.id or forecast.picking_type_id.default_location_dest_id.id,
            'company_id': forecast.company_id.id,
            'fiscal_position': supplier.property_account_position and supplier.property_account_position.id or False,
            'forecast_id': forecast.id,
            'notes': forecast.description,
            'picking_type_id': forecast.picking_type_id.id
        }

    def _prepare_purchase_order_line(self, cr, uid, forecast, forecast_line, purchase_id, supplier, context=None):
        if context is None:
            context = {}
        po_line_obj = self.pool.get('purchase.order.line')
        product_uom = self.pool.get('product.uom')
        product = forecast_line.product_id
        default_uom_po_id = product.uom_po_id.id
        ctx = context.copy()
        ctx['tz'] = forecast.create_by.tz
        date_order = forecast.date_create and fields.date.date_to_datetime(self, cr, uid, forecast.date_create, context=ctx) or fields.datetime.now()
        qty = product_uom._compute_qty(cr, uid, default_uom_po_id, forecast_line.qty_order, default_uom_po_id)
        supplier_pricelist = supplier.property_product_pricelist_purchase and supplier.property_product_pricelist_purchase.id or False
        vals = po_line_obj.onchange_product_id(
            cr, uid, [], supplier_pricelist, product.id, qty, default_uom_po_id,
            supplier.id, date_order=date_order,
            fiscal_position_id=supplier.property_account_position,
            date_planned=forecast_line.date,
            name=False, price_unit=False, state='draft', context=context)['value']
        vals.update({
            'order_id': purchase_id,
            'product_id': product.id,
            'forecast_line_id': forecast_line.id,
            #'account_analytic_id': forecast_line.account_analytic_id.id,
            'taxes_id': [(6, 0, vals.get('taxes_id', []))],
        })
        return vals
    
    def make_purchase_order_forecast(self, cr, uid, ids, partner_id, context=None):
        """
        Create New RFQ for Supplier
        """
        context = dict(context or {})
        assert partner_id, 'Supplier should be specified'
        purchase_order = self.pool.get('purchase.order')
        purchase_order_line = self.pool.get('purchase.order.line')
        forecast_line = self.pool.get('forecast.line')
        res_partner = self.pool.get('res.partner')
        supplier = res_partner.browse(cr, uid, partner_id, context=context)
        res = {}
        for forecast in self.browse(cr, uid, ids, context=context):
            context.update({'mail_create_nolog': True})
            purchase_id = purchase_order.create(cr, uid, self._prepare_purchase_order(cr, uid, forecast, supplier, context=context), context=context)
            purchase_order.message_post(cr, uid, [purchase_id], body=_("RFQ created"), context=context)
            res[forecast.id] = purchase_id
            for line in forecast.forecast_lines:
                purchase_line_id = purchase_order_line.create(cr, uid, self._prepare_purchase_order_line(cr, uid, forecast, line, purchase_id, supplier, context=context), context=context)
                forecast_line.write(cr, uid, line.id, {'purchase_id': purchase_id, 'purchase_line_id':purchase_line_id}, context=context)
        return res
    
class forecast_line(osv.osv):
    
    def _amount_line(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        if context is None:
            context = {}
        for line in self.browse(cr, uid, ids, context=context):
            res[line.id] = {
                'qty_order': 0.0,
                'price_subtotal': 0.0,
            }
            qty_order = line.qty_onhand - line.qty_out + line.qty_in - line.qty_min
            price_subtotal = line.price_unit * qty_order
            res[line.id]['qty_order'] = qty_order
            res[line.id]['price_subtotal'] = price_subtotal
        return res
    
    def _get_move_check(self, cr, uid, ids, name, args, context=None):
        res = {}
        for line in self.browse(cr, uid, ids, context=context):
            res[line.id] = bool(line.purchase_id) or bool(line.purchase_line_id)
        return res
    
    _name           = 'forecast.line'
    _description    = 'Line of Forecast'
    _columns        = {
        'name'               : fields.char('Name', size=64),
        'forecast_id'        : fields.many2one('stock.forecast', "Forecast"),
        'partner_id'        : fields.many2one('res.partner', "Supplier"),
        'location_id'        : fields.many2one('stock.location', 'Inventoried Location',),
        'product_category_id'   : fields.many2one('product.category', "Product Category"),
        'product_id'         : fields.many2one('product.product', "Product"),
        'qty_onhand'         : fields.float('SOH'),
        'qty_out'            : fields.float('Outgoing'),
        'qty_in'             : fields.float('Incoming'),
        'qty_min'             : fields.float('Buffer'),
        #'qty_order'          : fields.float('Rec. Order'),   
        'qty_order': fields.function(_amount_line, string='Rec. Order', multi='sums', digits_compute= dp.get_precision('Sale Order')),
        'price_unit'         : fields.float('Unit Price'),
        'price_subtotal': fields.function(_amount_line, string='Subtotal', multi='sums', digits_compute= dp.get_precision('Sale Order')),
        'date_from'          : fields.date('Date From'),
        'date_to'            : fields.date('Date To'),
        'date'               : fields.date('Date'),
        'procurement_id'     : fields.many2one('procurement.order', "Procurement"),
        'purchase_id': fields.many2one('purchase.order', 'Purchase Order'),
        'purchase_line_id': fields.many2one('purchase.order.line', 'Purchase Order Lines'),
        'parent_state': fields.related('forecast_id', 'state', type='char', string='State of Forecast'),
        'move_check': fields.function(_get_move_check, method=True, type='boolean', string='Posted', store=True)
    }
    
    _order = 'date asc'
    
    def _prepare_purchase_orderforecast_line(self, cr, uid, forecast, supplier, context=None):
        supplier_pricelist = supplier.property_product_pricelist_purchase
        return {
            'origin': forecast.name,
            'date_order': forecast.date_create or fields.datetime.now(),
            'partner_id': supplier.id,
            'pricelist_id': supplier_pricelist.id,
            'currency_id': supplier_pricelist and supplier_pricelist.currency_id.id or forecast.company_id.currency_id.id,
            'location_id': forecast.procurement_id and forecast.procurement_id.location_id.id or forecast.picking_type_id.default_location_dest_id.id,
            'company_id': forecast.company_id.id,
            'fiscal_position': supplier.property_account_position and supplier.property_account_position.id or False,
            'forecast_id': forecast.id,
            'notes': forecast.description,
            'picking_type_id': forecast.picking_type_id.id
        }

    def _prepare_purchase_order_lineforecast_line(self, cr, uid, forecast, forecast_line, purchase_id, supplier, context=None):
        if context is None:
            context = {}
        po_line_obj = self.pool.get('purchase.order.line')
        product_uom = self.pool.get('product.uom')
        product = forecast_line.product_id
        default_uom_po_id = product.uom_po_id.id
        ctx = context.copy()
        ctx['tz'] = forecast.create_by.tz
        date_order = forecast.date_create and fields.date.date_to_datetime(self, cr, uid, forecast.date_create, context=ctx) or fields.datetime.now()
        qty = product_uom._compute_qty(cr, uid, default_uom_po_id, forecast_line.qty_order, default_uom_po_id)
        supplier_pricelist = supplier.property_product_pricelist_purchase and supplier.property_product_pricelist_purchase.id or False
        vals = po_line_obj.onchange_product_id(
            cr, uid, [], supplier_pricelist, product.id, qty, default_uom_po_id,
            supplier.id, date_order=date_order,
            fiscal_position_id=supplier.property_account_position,
            date_planned=forecast_line.date,
            name=False, price_unit=False, state='draft', context=context)['value']
        vals.update({
            'order_id': purchase_id,
            'product_id': product.id,
            'forecast_line_id': forecast_line.id,
            #'account_analytic_id': forecast_line.account_analytic_id.id,
            'taxes_id': [(6, 0, vals.get('taxes_id', []))],
        })
        return vals
    
    def make_purchase_order_forecast_line(self, cr, uid, ids, partner_id, context=None):
        """
        Create New RFQ for Supplier
        """
        context = dict(context or {})
        assert partner_id, 'Supplier should be specified'
        purchase_order = self.pool.get('purchase.order')
        purchase_order_line = self.pool.get('purchase.order.line')
        res_partner = self.pool.get('res.partner')
        supplier = res_partner.browse(cr, uid, partner_id, context=context)
        res = {}
        for forecast in self.browse(cr, uid, ids, context=context):
            #print 'make_purchase_order_forecast_line',forecast,forecast.forecast_id
            context.update({'mail_create_nolog': True})
            purchase_id = purchase_order.create(cr, uid, self._prepare_purchase_orderforecast_line(cr, uid, forecast.forecast_id, supplier, context=context), context=context)
            purchase_order.message_post(cr, uid, [purchase_id], body=_("RFQ created"), context=context)
            res[forecast.id] = purchase_id
            #for line in forecast.forecast_lines:
            purchase_line_id = purchase_order_line.create(cr, uid, self._prepare_purchase_order_lineforecast_line(cr, uid, forecast.forecast_id, forecast, purchase_id, supplier, context=context), context=context)
            self.write(cr, uid, forecast.id, {'purchase_id': purchase_id, 'purchase_line_id':purchase_line_id}, context=context)
        return res
