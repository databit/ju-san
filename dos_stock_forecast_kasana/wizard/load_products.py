##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp
from openerp.tools.float_utils import float_round

class load_products(osv.osv_memory):
    _name = "load.products"
    _description = "Stock Forecast Load Products"

    _columns = {
        # ini ga ada fungsinya... biar keren aja
        'qtty': fields.float('Quantity', digits=(16, 2), required=False),
    }

    _defaults = {
        'qtty': 1.0,
    }

    def load_products(self, cr, uid, ids, context=None):
        """ create invoices for the active sales orders """
        product_categ_obj = self.pool.get('product.category')
        product_obj = self.pool.get('product.product')
        sale_obj = self.pool.get('sale.order')
        sale_line_obj = self.pool.get('sale.order.line')
        forecast_obj = self.pool.get('stock.forecast')
        forecast_line_obj = self.pool.get('forecast.line')
        forecast_ids = context.get('active_ids', [])
        product_ids = product_obj.search(cr, uid, [])
        res = {}
        for forecast in forecast_obj.browse(cr, uid, forecast_ids):
            #========================category/product child of=====================================
            categ_child_ids = forecast.product_category_id and product_categ_obj._get_children_and_consol(cr, uid, forecast.product_category_id and forecast.product_category_id.id, context=context)
            
            if categ_child_ids:
                product_ids=product_obj.search(cr,uid,[('categ_id','in',categ_child_ids)],context=context)
            elif forecast.product_category_id and forecast.product_id:
                product_ids=product_obj.search(cr,uid,[('id','=',forecast.product_id.id),('categ_id','=',forecast.product_category_id.id)],context=context)
            elif forecast.product_category_id and not forecast.product_id:
                product_ids=product_obj.search(cr,uid,[('categ_id','=',forecast.product_category_id.id)],context=context)    
            elif not forecast.product_category_id and forecast.product_id:
                product_ids=product_obj.search(cr,uid,[('id','=',forecast.product_id.id)],context=context)
            #=========================delete line===============================
            forecast_lines = []
            for fline in forecast.forecast_lines:
                forecast_lines.append(fline.id)
            if forecast_lines:
                forecast_line_obj.unlink(cr, uid, forecast_lines)
            #===================================================================
            domain_products = [('product_id', 'in', product_ids)]
            domain_quant, domain_move_in, domain_move_out = [], [], []
            context.update({'location': forecast.location_id and forecast.location_id.id or []})
            #print "product_ids",product_ids
            domain_quant_loc, domain_move_in_loc, domain_move_out_loc = product_obj._get_domain_locations(cr, uid, product_ids, context=context)
            #print "====",domain_quant_loc, domain_move_in_loc, domain_move_out_loc
            context.update({'from_date': forecast.date_from or False,'to_date': forecast.date_to or False})
            domain_move_in += product_obj._get_domain_dates(cr, uid, product_ids, context=context) + [('state', 'not in', ('done', 'cancel', 'draft'))] + domain_products
            domain_move_out += product_obj._get_domain_dates(cr, uid, product_ids, context=context) + [('state', 'not in', ('done', 'cancel', 'draft'))] + domain_products
            domain_quant += domain_products
    
            if context.get('lot_id'):
                domain_quant.append(('lot_id', '=', context['lot_id']))
            if context.get('owner_id'):
                domain_quant.append(('owner_id', '=', context['owner_id']))
                owner_domain = ('restrict_partner_id', '=', context['owner_id'])
                domain_move_in.append(owner_domain)
                domain_move_out.append(owner_domain)
            if context.get('package_id'):
                domain_quant.append(('package_id', '=', context['package_id']))
    
            domain_move_in += domain_move_in_loc
            domain_move_out += domain_move_out_loc
            moves_in = self.pool.get('stock.move').read_group(cr, uid, domain_move_in, ['product_id', 'product_qty'], ['product_id'], context=context)
            moves_out = self.pool.get('stock.move').read_group(cr, uid, domain_move_out, ['product_id', 'product_qty'], ['product_id'], context=context)
    
            domain_quant += domain_quant_loc
            quants = self.pool.get('stock.quant').read_group(cr, uid, domain_quant, ['product_id', 'qty'], ['product_id'], context=context)
            quants_loc = self.pool.get('stock.quant').search(cr, uid, domain_quant, context=context)
            quant_result = []
            for quant in self.pool.get('stock.quant').browse(cr, uid, quants_loc):
                res = {}
                res['product_id'] = quant.product_id and quant.product_id.id
                res['qty'] = quant.qty
                res['buffer_available'] = quant.product_id and quant.product_id.buffer_available
                res['location_id'] = quant.location_id and quant.location_id.id
                res['categ_id'] = quant.product_id.categ_id and quant.product_id.categ_id.id
                res['price_unit'] = quant.product_id.list_price or 0.0
                res['partner_forecast_id'] = quant.product_id.partner_forecast_id and quant.product_id.partner_forecast_id.id
                quant_result.append(res)
            #print "quants11111111",quant_result
            quants = dict(map(lambda x: (x['product_id'][0], x['qty']), quants))
            #print "quants222222222",quants
            moves_in = dict(map(lambda x: (x['product_id'][0], x['product_qty']), moves_in))
            moves_out = dict(map(lambda x: (x['product_id'][0], x['product_qty']), moves_out))
            for product in product_obj.browse(cr, uid, product_ids, context=context):
                id = product.id
                qty_available = price_unit = buffer_available = 0.0
                location_id = False
                categ_id = False
                partner_forecast_id = False
                for prod in quant_result:
                    if id == prod['product_id']:
                        qty_available += prod['qty']
                        buffer_available += prod['buffer_available']
                        location_id = prod['location_id']
                        categ_id = prod['categ_id']
                        price_unit = prod['price_unit']
                        partner_forecast_id = prod['partner_forecast_id']
                #qty_available = float_round(quants.get(id, 0.0), precision_rounding=product.uom_id.rounding)
                incoming_qty = float_round(moves_in.get(id, 0.0), precision_rounding=product.uom_id.rounding)
                outgoing_qty = float_round(moves_out.get(id, 0.0), precision_rounding=product.uom_id.rounding)
                virtual_available = float_round(quants.get(id, 0.0) + moves_in.get(id, 0.0) - moves_out.get(id, 0.0), precision_rounding=product.uom_id.rounding)
                #recommend_qty = float_round(qty_available - outgoing_qty + incoming_qty - , precision_rounding=product.uom_id.rounding)
                res[id] = {
                    'qty_available': qty_available,
                    'incoming_qty': incoming_qty,
                    'outgoing_qty': outgoing_qty,
                    'virtual_available': virtual_available,
                    'buffer_available': buffer_available,
                }
                if (qty_available + incoming_qty + outgoing_qty + virtual_available) > 0.0:
                    #print "qty_available",qty_available,incoming_qty,outgoing_qty,virtual_available
                    lines = {
                        'product_id': id,
                        'partner_id': partner_forecast_id,
                        'product_category_id': categ_id,
                        'forecast_id': forecast and forecast.id,
                        'location_id': location_id or forecast.location_id and forecast.location_id.id,
                        'qty_onhand':res[id]['qty_available'],
                        'qty_out':res[id]['outgoing_qty'],
                        'qty_in':res[id]['incoming_qty'],
                        'qty_min':res[id]['buffer_available'],
                        'price_unit': price_unit,
                    }
                    forecast_line_obj.create(cr, uid, lines)
        return {'type': 'ir.actions.act_window_close'}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
