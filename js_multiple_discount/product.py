from openerp.osv import osv, fields, expression

class product_template(osv.osv):
    _inherit = "product.template"
    _description = "Product Template"    
    _columns = {
        'supplier_discount_ids': fields.one2many('product.supplier.discount', 'product_tmpl_id', 'Supplier Discount'),
        'customer_discount_ids': fields.one2many('product.customer.discount', 'product_tmpl_id', 'Customer Discount'),
    }
    
class product_supplier_discount(osv.osv):
    _name = "product.supplier.discount"
    _description = "Information about a discount multiple from supplier"

    _columns = {
        'product_tmpl_id' : fields.many2one('product.template', 'Product Template', required=True, ondelete='cascade', select=True, oldname='product_id'),
        'name' : fields.integer('Sequence', help="Assigns the priority to the list of product supplier."),
        'discount': fields.float('Discount', required=True, help="The discount information from supplier"),
    }
    _order = 'name'
    
#     _sql_constraints = [
#         ('name_uniq', 'unique (product_tmpl_id, name)', 'Sequence of discount suppliers should be unique!')
#     ]


class product_customer_discount(osv.osv):
    
    def _show_discount(self, cr, uid, ids, name, args, context=None):
        res = {}
        for line in self.browse(cr, uid, ids, context=context):
            res[line.id] = ''
            if line.discount_ids:
                wheres = []
                for disc in line.discount_ids:
                    wheres.append(str(disc.name)+'%')
                if len(wheres) == 1:
                    res[line.id] = "".join(wheres)
                else:
                    res[line.id] = ", ".join(wheres)
        return res
    
    _name = "product.customer.discount"
    _description = "Information about a discount multiple to customer"
    
    _columns = {
        'product_tmpl_id' : fields.many2one('product.template', 'Product Template', required=True, ondelete='cascade', select=True, oldname='product_id'),
        'name' : fields.many2one('res.partner.group', 'Partner Groups', required=True, help="Groups customer of discount"),
        'show_discount': fields.function(_show_discount, string='Discount Multiple', type='char', readonly=True),
        'discount_ids': fields.one2many('discount.multiple', 'product_customer_id','Discount Multiple', required=True, help="The discount multiple for customer groups"),
    }
        
class discount_multiple(osv.osv):
    _name = "discount.multiple"
    _description = "Information about a discount multiple to customer"

    _columns = {
        'product_customer_id' : fields.many2one('product.customer.discount', 'Discount Customer', required=False),
        'name' : fields.float('Discount', required=True, help="Assigns the priority to the list of discount customer."),
        'sequence': fields.integer('Sequence', help="The discount information for customer"),
    }
    
#     _sql_constraints = [
#         ('sequence_uniq', 'unique (product_customer_id, sequence)', 'Sequence of discount multiple for customer should be unique!')
#     ]
    