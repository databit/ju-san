from osv import fields, osv
from tools.translate import _

class res_company(osv.osv):
    _inherit = 'res.company'
    _columns = {
	      'npwp' : fields.char('NPWP', size=20, required=False),
    }
res_company()